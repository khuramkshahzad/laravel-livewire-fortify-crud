<div>
    <div class="-my-2 bg-white border-b border-gray-200">

        @if(session()->has('message'))
            <div class="flex items-center bg-green-400 text-white text-sm font-bold px-4 py-3 relative" role="alert"
                 x-data="{show: true}" x-show="show">
                <svg class="fill-current w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                    <path d="M12.432 0c1.34 0 2.01.912 2.01 1.957 0 1.305-1.164 2.512-2.679 2.512-1.269 0-2.009-.75-1.974-1.99C9.789 1.436 10.67 0 12.432 0zM8.309 20c-1.058 0-1.833-.652-1.093-3.524l1.214-5.092c.211-.814.246-1.141 0-1.141-.317 0-1.689.562-2.502 1.117l-.528-.88c2.572-2.186 5.531-3.467 6.801-3.467 1.057 0 1.233 1.273.705 3.23l-1.391 5.352c-.246.945-.141 1.271.106 1.271.317 0 1.357-.392 2.379-1.207l.6.814C12.098 19.02 9.365 20 8.309 20z"/>
                </svg>
                <p>{{ session('message') }}</p>
                <span class="absolute top-0 bottom-0 right-0 px-4 py-3" @click="show = false">
            <svg class="fill-current h-6 w-6 text-white" role="button" xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 20 20"><title>Close</title><path
                        d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
        </span>
            </div>
        @endif
    </div>

    <div class="block mb-8 mt-8">
        <x-jet-button wire:click="confirmUserAdd"
                      class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
            {{ __('Add New User') }}
        </x-jet-button>
    </div>
    <div class="flex flex-col">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">

            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-200 w-full">
                        <thead>
                        <tr>
                            <th scope="col" width="50"
                                class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                <div class="flex items-center">
                                    <button wire:click="sortBy('id')">#</button>
                                    <x-sort-icon sortField="id" :sort-by="$sortBy" :sort-asc="$sortAsc"/>
                                </div>
                            </th>
                            <th scope="col"
                                class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                <div class="flex items-center">
                                    <button wire:click="sortBy('name')">Name</button>
                                    <x-sort-icon sortField="name" :sort-by="$sortBy" :sort-asc="$sortAsc"/>
                                </div>
                            </th>
                            <th scope="col"
                                class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                <div class="flex items-center">
                                    <button wire:click="sortBy('email')">Email</button>
                                    <x-sort-icon sortField="email" :sort-by="$sortBy" :sort-asc="$sortAsc"/>
                                </div>
                            </th>
                            <th scope="col" width="200" class="px-6 py-3 bg-gray-50">

                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                        @foreach ($users as $user)
                            <tr>
                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $loop->iteration }}
                                </td>

                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $user->name }}
                                </td>

                                <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-900">
                                    {{ $user->email }}
                                </td>

                                <td class="px-6 py-4 whitespace-nowrap text-sm font-medium">
                                    <x-jet-button wire:click="confirmUserEdit( {{ $user->id}})"
                                                  class="bg-green-500 hover:bg-green-700">
                                        Edit
                                    </x-jet-button>
                                    @if(auth()->user()->id != $user->id)
                                        <x-jet-danger-button wire:click="confirmUserDeletion( {{ $user->id}})"
                                                             wire:loading.attr="disabled">
                                            Delete
                                        </x-jet-danger-button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="p-5">
        {{ $users->links() }}
    </div>

    <x-jet-confirmation-modal wire:model="confirmingUserDeletion">
        <x-slot name="title">
            {{ __('Delete User') }}
        </x-slot>

        <x-slot name="content">
            {{ __('Are you sure you want to delete User? ') }}
        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$set('confirmingUserDeletion', false)" wire:loading.attr="disabled">
                {{ __('Cancel') }}
            </x-jet-secondary-button>

            <x-jet-danger-button class="ml-2" wire:click="deleteUser({{ $confirmingUserDeletion }})"
                                 wire:loading.attr="disabled">
                {{ __('Delete') }}
            </x-jet-danger-button>
        </x-slot>
    </x-jet-confirmation-modal>

    <x-jet-dialog-modal wire:model="confirmingUserAdd">
        <x-slot name="title">
            {{ isset( $this->user->id) ? 'Edit User' : 'Add User'}}
        </x-slot>

        <x-slot name="content">
            <div class="col-span-6 sm:col-span-4">
                <x-jet-label for="name" value="{{ __('Name') }}"/>
                <x-jet-input id="name" type="text" class="mt-1 block w-full" wire:model.defer="user.name"/>
                <x-jet-input-error for="user.name" class="mt-2"/>
            </div>

            <div class="col-span-6 sm:col-span-4 mt-4">
                <x-jet-label for="email" value="{{ __('Email') }}"/>
                <x-jet-input id="email" type="email" class="mt-1 block w-full" wire:model.defer="user.email"/>
                <x-jet-input-error for="user.email" class="mt-2"/>
            </div>

            @if(!isset($this->user->id))
                <div class="col-span-6 sm:col-span-4 mt-4">
                    <x-jet-label for="password" value="{{ __('Password') }}"/>
                    <x-jet-input id="password" type="password" class="mt-1 block w-full"
                                 wire:model="user.password"/>
                    <x-jet-input-error for="user.password" class="mt-2"/>
                </div>

                <div class="col-span-6 sm:col-span-4 mt-4">
                    <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}"/>
                    <x-jet-input id="password_confirmation" type="password" class="mt-1 block w-full"
                                 wire:model="user.password_confirmation"/>
                    <x-jet-input-error for="user.password_confirmation" class="mt-2"/>
                </div>
            @endif

        </x-slot>

        <x-slot name="footer">
            <x-jet-secondary-button wire:click="$set('confirmingUserAdd', false)" wire:loading.attr="disabled">
                {{ __('Cancel') }}
            </x-jet-secondary-button>

            <x-jet-button wire:click="saveUser()" wire:loading.attr="disabled"
                          class="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded ml-2">
                {{ __('Save') }}
            </x-jet-button>

        </x-slot>
    </x-jet-dialog-modal>
</div>