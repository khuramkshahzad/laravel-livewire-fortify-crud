<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Fortify\Rules\Password;
use Livewire\Component;
use Livewire\WithPagination;

class ShowUsers extends Component
{
    use WithPagination;

    public $per_page = 5;
    public $sortBy = 'id';
    public $sortAsc = true;
    public $user;

    public $confirmingUserDeletion = false;
    public $confirmingUserAdd = false;

    protected $rules = [
        'user.name' => 'required|string|max:255',
        'user.email' => 'required|email|max:255|unique:users,email',
        'user.password' => 'required|confirmed|' . Password::class,
    ];

    public function setAddUserRules()
    {
        $this->rules = [
            'user.name' => 'required|string|max:255',
            'user.email' => 'required|email|max:255|unique:users,email',
            'user.password' => ['required', 'string', new Password, 'confirmed']
        ];
    }

    public function setEditUserRules($user)
    {
        $this->rules = [
            'user.name' => 'required|string|max:255',
            'user.email' => "required|email|max:255|unique:users,id,{$user->id}",
        ];
    }

    public function render()
    {
        $users = User::orderBy($this->sortBy, $this->sortAsc ? 'ASC' : 'DESC');

        $users = $users->paginate($this->per_page);

        return view('livewire.show-users', [
            'users' => $users,
        ]);
    }

    public function updated($propertyName)
    {
        $this->validateOnly(
            $propertyName,
            ['user.password' => ['required', 'string', new Password, 'confirmed']],
            [],
            ['user.password' => ' Password'],
        );
    }

    public function sortBy($field)
    {
        if ($field == $this->sortBy) {
            $this->sortAsc = !$this->sortAsc;
        }
        $this->sortBy = $field;
    }

    public function confirmUserDeletion($id)
    {
        $this->confirmingUserDeletion = $id;
    }

    public function deleteUser(User $user)
    {
        $user->delete();
        $this->confirmingUserDeletion = false;
        session()->flash('message', 'User Deleted Successfully');
    }

    public function confirmUserAdd()
    {
        $this->reset(['user']);
        $this->confirmingUserAdd = true;
    }

    public function confirmUserEdit(User $user)
    {
        $this->resetErrorBag();
        $this->user = $user;
        $this->confirmingUserAdd = true;
    }

    public function saveUser()
    {
        if (isset($this->user->id)) {

            $this->setEditUserRules($this->user);
            $this->validate();

            User::where('id', $this->user->id)->update([
                'name' => $this->user['name'],
                'email' => $this->user['email'],
            ]);
            session()->flash('message', 'User Saved Successfully');

        } else {

            $this->setAddUserRules();
            $this->validate();

            User::create([
                'name' => $this->user['name'],
                'email' => $this->user['email'],
                'password' => Hash::make($this->user['password']),
            ]);
            session()->flash('message', 'User Added Successfully');
        }

        $this->confirmingUserAdd = false;
    }
}
