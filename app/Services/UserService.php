<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class UserService.
 */
class UserService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @param array $data
     *
     * @return User
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): ?User
    {
        DB::beginTransaction();

        try {
            $this->model->name = $data['name'] ?? null;
            $this->model->email = $data['email'] ?? null;
            $this->model->password = $data['password'] ?? null;

            $this->model->save();

        } catch (Exception $e) {
            DB::rollBack();

            return null;
        }

        DB::commit();
        return $this->model;
    }

    /**
     * @param User $user
     * @param array $data
     *
     * @return User
     * @throws \Throwable
     */
    public function update(User $user, array $data = []): ?User
    {
        DB::beginTransaction();

        try {
            $user->update([
                'name' => $data['name'] ?? null,
            ]);

        } catch (Exception $e) {
            DB::rollBack();

            return null;
        }

        DB::commit();

        return $user;
    }

    /**
     * @param User $user
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(User $user): bool
    {
        if ($user->forceDelete()) {

            return true;
        }

        return false;
    }
}
