<?php

namespace App\Services;

use App\Exceptions\GeneralException;
use App\Services\BaseService;
use App\Models\Article;
use Exception;
use Illuminate\Support\Facades\DB;

/**
 * Class ArticleService.
 */
class ArticleService extends BaseService
{
    /**
     * ArticleService constructor.
     *
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->model = $article;
    }

    /**
     * @param array $data
     *
     * @return Article
     * @throws GeneralException
     * @throws \Throwable
     */
    public function store(array $data = []): ?Article
    {
        DB::beginTransaction();

        try {
            $this->model->title = $data['title'] ?? null;
            $this->model->description = $data['description'] ?? null;

            $this->model->save();


        } catch (Exception $e) {
            DB::rollBack();

            return null;
        }

        DB::commit();
        return $this->model;
    }

    /**
     * @param Article $article
     * @param array $data
     *
     * @return Article
     * @throws \Throwable
     */
    public function update(Article $article, array $data = []): ?Article
    {
        DB::beginTransaction();

        try {
            $article->update([
                'title' => $data['title'] ?? null,
                'description' => $data['description'] ?? null,
            ]);

        } catch (Exception $e) {
            DB::rollBack();

            return null;
        }

        DB::commit();

        return $article;
    }

    /**
     * @param Article $article
     *
     * @return bool
     * @throws GeneralException
     */
    public function destroy(Article $article): bool
    {
        if ($article->forceDelete()) {

            return true;
        }

        return false;
    }
}
